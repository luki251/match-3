﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//game engine
public class Engine : MonoBehaviour {

	private Board board;
	private float nextUpdate = 0;

	void Start () {
		board = gameObject.AddComponent <Board>() as Board;
	}

	void Update () {
		if (Time.time > nextUpdate) {
			CheckVertically ();
			CheckHorizontally ();
			Game ();
			nextUpdate += Config.TimeInterval;
		}
	}
	
	//generates blocks and swaps random blocks if board is full
	void Game() {
		if (board.EmptySlots > 0) {
			int indexX = Random.Range (0, Config.BoardSizeX);
			while (board.IsStackFull (indexX)) {
				indexX = Random.Range (0, Config.BoardSizeX);
			}
			int randomColor = Random.Range (0, Config.colors.Length);
			board.AddBlock (indexX, Config.colors [randomColor]);			
		}
		else {
			bool actionDone = false;
			int indexX = Random.Range (0, Config.BoardSizeX);
			int indexY = Random.Range (0, Config.BoardSizeY);
			while (!actionDone) {
				int shift = Random.Range(0,3);
				switch (shift) {
				case 0:
					actionDone = board.SwapBlocks (indexX, indexY, indexX + 1, indexY);
					break;
				case 1:
					actionDone = board.SwapBlocks(indexX,indexY,indexX-1,indexY);
					break;
				case 2:
					actionDone = board.SwapBlocks(indexX,indexY,indexX,indexY+1);
					break;
				case 3:
					actionDone = board.SwapBlocks(indexX,indexY,indexX,indexY-1);
					break;
				}
			}
		}
	}
	
	//methods finding matching blocks
	private void CheckVertically()
	{
		int match = 1;
		int maxMatch = 0;
		int[] maxMatchIndexes = new int[2];
		Color color = new Color();
		Color previousColor = new Color();

		for (int indexX = 0; indexX < Config.BoardSizeX; indexX++) {			
			for (int indexY = 0; indexY < Config.BoardSizeY; indexY++) {
				if (board.AmountOfBlocksInStack (indexX) > indexY) {

					color = board.GetBlockColor (indexX, indexY);
					if (color == previousColor) {
						match += 1;
						if (match > maxMatch) {
							maxMatch = match;
							maxMatchIndexes [0] = indexX;
							maxMatchIndexes [1] = indexY; 
						}
					} else {
						match = 1;
					}
					previousColor = color;
				}
				if (indexX > 0 && board.AmountOfBlocksInStack (indexX) < board.AmountOfBlocksInStack (indexX - 1))
					match = 1;
			}
			previousColor =  new Color();
			match = 1;
		}

		if (maxMatch >= Config.Match) {
			for (int indexY = maxMatchIndexes [1]; indexY > maxMatchIndexes [1] - maxMatch; indexY--) {
				board.RemoveBlock (maxMatchIndexes [0], indexY);			
			}
		}
	}

	private void CheckHorizontally()
	{
		int match = 1;
		int maxMatch = 0;
		int[] maxMatchIndexes = new int[2];
		Color color = new Color();
		Color previousColor = new Color();

		for (int indexY = 0; indexY < Config.BoardSizeY; indexY++) {

			for (int indexX = 0; indexX < Config.BoardSizeX; indexX++) {
				bool isEmpty = false;
				try{
				color = board.GetBlockColor(indexX,indexY);
				}
				catch{
					isEmpty = true;
					color =  new Color();
					//print ("empty");
				}

				if (color == previousColor && !isEmpty) {
					match += 1;
					if(match > maxMatch)
					{
						maxMatch = match;
						maxMatchIndexes [0] = indexX;
						maxMatchIndexes [1] = indexY; 
					}
				} else {
					match = 1;
				}
				previousColor = color;

			}
			previousColor =  new Color();
			match = 1;
		}

		if (maxMatch >= Config.Match) {
			for (int indexX = maxMatchIndexes [0]; indexX > maxMatchIndexes [0] - maxMatch; indexX--) {
				board.RemoveBlock (indexX, maxMatchIndexes [1]);				
			}
		}
	}
}
