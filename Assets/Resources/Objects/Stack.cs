﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//represents stack of blocks (single column on the game board)
public class Stack : MonoBehaviour {

	private Vector3 position;
	private List<Block> blocks = new List<Block>();

	public int AmountOfBlocks()
	{
		return blocks.Count;
	}

	void Start () {
		Object StackPrefab = Resources.Load (Config.StackPrefabPath);
		Instantiate(StackPrefab, position, transform.rotation);
	}

	public Stack Initiate(Vector3 position)
	{		
		this.position = position;
		return this;
	}

	public bool IsFull() { 
		if (AmountOfBlocks() >= Config.BoardSizeY) {
			return true;
		} else {
			return false;
		}
	}

	public Color GetBlockColor(int indexY)
	{
		if(AmountOfBlocks() <= indexY && indexY < 0)
			throw new UnityException("Block is null");
		return blocks[indexY].BlockColor;
	}

	public void AddBlock(Color color)
	{		
		if (!IsFull ()) {
			blocks.Add (gameObject.AddComponent <Block> ().Initiate (position, color) as Block);
		} else
			throw new UnityException("Stack is full");
	}

	public void RemoveBlock(int index)
	{
		if (AmountOfBlocks() > index) {
			Destroy (blocks[index]);
			blocks.RemoveAt (index);
		} else
			throw new UnityException("Index out of range");
	}

	public void SetBlockColor(int index, Color color){		
		if (AmountOfBlocks() > index) {
			blocks[index].BlockColor = color;
		}
		else
			throw new UnityException("Index out of range");
	}
}
