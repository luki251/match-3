﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//constant values used in project
public static class Config {
	public const string BlockPrefabPath = "Prefabs/Cube/CubePrefab";
	public const string BlockMaterialPath = "Prefabs/Cube/CubeMaterial";
	public const string StackPrefabPath = "Prefabs/Field/FieldPrefab";

	public static Color[] colors = new Color[6]{Color.red,Color.green,Color.blue,Color.cyan,Color.magenta,Color.yellow};	

	public const int BoardSizeX = 5; //width
	public const int BoardSizeY = 5; //height

	public const int BlockSpawnHeihgt = 6;

	public const float TimeInterval = 1.0f;

	public const int Match = 3; //how many blocks have to match
}
