﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//represents game board
public class Board : MonoBehaviour {

	private Stack[] stacks = new Stack[Config.BoardSizeX];
	private int emptySlots = Config.BoardSizeX * Config.BoardSizeY;

	void Start () {
		for (int indexX = 0; indexX < Config.BoardSizeX; indexX++) {			
				stacks[indexX] = gameObject.AddComponent <Stack> ().Initiate (new Vector3 (indexX, 0, 0)) as Stack;
		}
	}

	public int EmptySlots{
		get{ return emptySlots;}
	}

	public Color GetBlockColor(int indexX, int indexY)
	{	
		Color blockColor;	
		try{
			blockColor = stacks [indexX].GetBlockColor (indexY);
		}
		catch{
			throw new UnityException("Block is null");
		}
		return blockColor;
	}

	public int AmountOfBlocksInStack(int indexX)
	{
		if (indexX < Config.BoardSizeX && indexX > -1) {
			return stacks [indexX].AmountOfBlocks ();
		} else
			throw new UnityException ("Index out of range");
	}

	public bool IsStackFull(int indexX)
	{
		if (indexX < Config.BoardSizeX && indexX > -1) {
			if (stacks [indexX].IsFull ())
				return true;
			else
				return false;
		} else
			throw new UnityException ("Index out of range");
	}

	public void AddBlock(int indexX, Color color)
	{		
		try{
			stacks [indexX].AddBlock (color);
			emptySlots -= 1;
		}
		catch{
			throw new UnityException ("Index out of range");
		}
	}

	public void RemoveBlock(int indexX, int indexY)
	{
		try{
			stacks [indexX].RemoveBlock (indexY);
			emptySlots += 1;
		}
		catch{
			throw new UnityException ("Index out of range");
		}
	}

	public bool SwapBlocks(int indexX1, int indexY1, int indexX2, int indexY2)
	{		
		bool outOfRange = false;
		try{
			stacks [indexX1].GetBlockColor (indexY1);
		}
		catch {
			outOfRange = true;
		}
		try{
			stacks [indexX2].GetBlockColor (indexY2);
		}
		catch {
			outOfRange = true;
		}

		if (!outOfRange) {
			Color colorSwap = stacks [indexX1].GetBlockColor (indexY1);
			stacks [indexX1].SetBlockColor (indexY1, stacks [indexX2].GetBlockColor (indexY2));
			stacks [indexX2].SetBlockColor (indexY2, colorSwap);
			return true;
		} else
			return false;
	}
}
