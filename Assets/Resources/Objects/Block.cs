﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//represents sigle block
public class Block : MonoBehaviour {

	private GameObject blockGameObject;
	private Vector3 position;
	private Color blockColor;
	private Material blockMaterial;

	public Color BlockColor{
		get {return blockColor;}
		set{ 
			blockColor = value;
			blockMaterial.color = value;
		}
	}

	void Start () {
		blockMaterial = new Material((Material)Resources.Load (Config.BlockMaterialPath));
		blockMaterial.color = blockColor;

		Object BlockPrefab = Resources.Load (Config.BlockPrefabPath);
		blockGameObject = (GameObject)Instantiate (BlockPrefab, position, transform.rotation);
		blockGameObject.GetComponent<Renderer>().material = blockMaterial;
	}

	public Block Initiate(Vector3 position, Color color)
	{		
		this.position = position;
		this.position [1] += Config.BlockSpawnHeihgt;
		blockColor = color;
		return this;
	}

	void OnDestroy() {
		Destroy (blockGameObject);
	}
}
